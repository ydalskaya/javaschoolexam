package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (isIncorrect(statement)) {
            return null;
        }

        Deque<Double> stackNum = new ArrayDeque<>();
        Deque<Character> stackOp = new ArrayDeque<>();

        statement = statement.replace(" ", "");
        String[] elements = statement.split("(?<=[-+*/()])|(?=[-+*/()])"); //Array with numbers and operations in it
        statement = "";
        for (String s : elements) {
            statement += s + " ";
        }
        statement = statement.replaceAll("^-\\s", "-");
        elements = statement.split(" ");


        for (int i = 0; i < elements.length; i++) {
            try {
                stackNum.push(Double.parseDouble(elements[i]));
            } catch (NumberFormatException e) {
                if (stackOp.isEmpty() || elements[i].charAt(0) == '('
                        || stackOp.peek() == '(' || getPriority(elements[i].charAt(0)) > getPriority(stackOp.peek())) {
                    stackOp.push(elements[i].charAt(0));
                } else { //If priority is <= or elements[i].charAt(0) == ')'
                    if (elements[i].charAt(0) == ')') {
                        while (stackOp.peek() != '(') {
                            stackNum.push(calculate(stackNum.pop(), stackNum.pop(), stackOp.pop()));
                        }
                        stackOp.pop();
                        continue;
                    }
                    while (!stackOp.isEmpty() && getPriority(elements[i].charAt(0)) <= getPriority(stackOp.peek()) && stackOp.peek() != '(') {
                        stackNum.push(calculate(stackNum.pop(), stackNum.pop(), stackOp.pop()));
                    }
                    stackOp.push(elements[i].charAt(0));
                }
            }
        }

        while (stackNum.size() != 1) {
            stackNum.push(calculate(stackNum.pop(), stackNum.pop(), stackOp.pop()));
        }
        double dResult = stackNum.pop();
        if (dResult == Double.POSITIVE_INFINITY || dResult == Double.NEGATIVE_INFINITY || dResult == Double.NaN)
            return null;
        return roundResult(dResult);
    }


    public double calculate(double y, double x, char op) {
        switch (op) {
            case '+':
                return x + y;
            case '-':
                return x - y;
            case '*':
                return x * y;
            default:
                return x / y;
        }
    }


    public int getPriority(char operation) {
        int priority = 0;
        switch (operation) {
            case '+':
            case '-':
                priority = 1;
                break;
            case '*':
            case '/':
                priority = 2;
                break;
        }
        return priority;
    }


    public boolean isIncorrect(String input) {
        if (input == null || input.isEmpty()) {
            return true;
        } else if (input.concat(" ").split("([^0-9\\-+*/().\\s])").length > 1) {
            return true;
        } else if (input.contains("..")
                || input.contains("--")
                || input.contains("++")
                || input.contains("**")
                || input.contains("//")
                || input.matches(".*[\\-+*//]")) return true;
        else if (checkParentheses(input)) {
            return true;
        } else {
            return false;
        }
    }


    public boolean checkParentheses(String input) {
        int opened = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '(') {
                opened++;
            } else if (input.charAt(i) == ')') {
                if (opened == 0) {
                    return true;
                }
                opened--;
            }
        }
        return opened != 0;
    }

    public String roundResult(double dResult) {
        DecimalFormatSymbols setDot = new DecimalFormatSymbols(Locale.getDefault());
        setDot.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#.####", setDot);
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        return decimalFormat.format(dResult);
    }
}
