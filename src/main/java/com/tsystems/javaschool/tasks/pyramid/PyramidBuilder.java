package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] pyramid;

        double x = (-1 + Math.sqrt(1 + 8 * inputNumbers.size())) / 2; //Count of rows

        if (x % (int) x > 0 || Double.isNaN(x) || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        else {
            Collections.sort(inputNumbers);
            int countOfRows = (int) x;
            int sizeOfRow = countOfRows * 2 - 1;
            int num = 0; //Current item's number to take from the list
            pyramid = new int[countOfRows][sizeOfRow];
            int position;

            for (int i = 0; i < countOfRows; i++) {
                position = (sizeOfRow - (2 * i + 1)) / 2; //Position to insert
                for (int j = 0; j < i + 1; j++) {
                    pyramid[i][position] = inputNumbers.get(num++);
                    if (j + 1 < i + 1) {
                        position += 2;
                    }
                }
            }
        }
        return pyramid;
    }
}
